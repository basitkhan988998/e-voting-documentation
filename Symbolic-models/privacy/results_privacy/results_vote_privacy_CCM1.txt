Biprocess 0 (that is, the initial process):
(
    {1}let id: agent_id = idA in
    {2}let is_target_voter: bool = true in
    {3}in(c, VCks_id: bitstring);
    {4}let SVK_id: password = SVK(id) in
    {5}let KSkey_id: symmetric_ekey = KSkey(SVK_id) in
    {6}let Sk_id: private_ekey = catch-fail(Dec_s(VCks_id,KSkey_id)) in
    {7}let v_1: bool = not-caught-fail(Sk_id) in
    {8}let Sk_id_1: private_ekey = (if v_1 then Sk_id else fail-any) in
    {9}if is_target_voter then
    (
        {10}let (hpCCA1: num,hpCCB1: num) = (H(pCC(Sk_id_1,v(jA1))),H(pCC(Sk_id_1,v(jB1)))) in
        {11}let (lpCCA1: num,lpCCB1: num) = (H((hpCCA1,VC(id))),H((hpCCB1,VC(id)))) in
        {12}let pCA1: num = tild(kCCR(VC(id)),H(pCC(Sk_id_1,v(jA1)))) in
        {13}let pCB1: num = tild(kCCR(VC(id)),H(pCC(Sk_id_1,v(jB1)))) in
        {14}let lCCA1: num = H(H((VC(id),pCA1))) in
        {15}let lCCB1: num = H(H((VC(id),pCB1))) in
        {16}let pVCC: num = tild(kCCR'(VC(id)),H(tild(Sk_id_1,sq(BCK(id))))) in
        {17}let lVCC: num = H(H((VC(id),pVCC))) in
        (
            {18}out(c, choice[lpCCA1,lpCCB1])
        ) | (
            {19}out(c, choice[lpCCB1,lpCCA1])
        ) | (
            {20}out(c, choice[lCCA1,lCCB1])
        ) | (
            {21}out(c, choice[lCCB1,lCCA1])
        ) | (
            {22}out(c, lVCC)
        )
    ) | (
        {23}!
        {24}in(c, J: num);
        {25}if (not(is_target_voter) || ((J ≠ jA1) && (J ≠ jB1))) then
        {26}let hpCC: num = H(pCC(Sk_id_1,v(J))) in
        {27}let lpCC: num = H((hpCC,VC(id))) in
        {28}let pC: num = tild(kCCR(VC(id)),H(pCC(Sk_id_1,v(J)))) in
        {29}let lCC: num = H(H((VC(id),pC))) in
        {30}let pVCC_1: num = tild(kCCR'(VC(id)),H(tild(Sk_id_1,sq(BCK(id))))) in
        {31}let lVCC_1: num = H(H((VC(id),pVCC_1))) in
        {32}out(c, (lpCC,lCC,lVCC_1))
    )
) | (
    {33}let id_1: agent_id = idB in
    {34}let is_target_voter_1: bool = true in
    {35}in(c, VCks_id_1: bitstring);
    {36}let SVK_id_1: password = SVK(id_1) in
    {37}let KSkey_id_1: symmetric_ekey = KSkey(SVK_id_1) in
    {38}let Sk_id_2: private_ekey = catch-fail(Dec_s(VCks_id_1,KSkey_id_1)) in
    {39}let v_2: bool = not-caught-fail(Sk_id_2) in
    {40}let Sk_id_3: private_ekey = (if v_2 then Sk_id_2 else fail-any) in
    {41}if is_target_voter_1 then
    (
        {42}let (hpCCA1_1: num,hpCCB1_1: num) = (H(pCC(Sk_id_3,v(jA1))),H(pCC(Sk_id_3,v(jB1)))) in
        {43}let (lpCCA1_1: num,lpCCB1_1: num) = (H((hpCCA1_1,VC(id_1))),H((hpCCB1_1,VC(id_1)))) in
        {44}let pCA1_1: num = tild(kCCR(VC(id_1)),H(pCC(Sk_id_3,v(jA1)))) in
        {45}let pCB1_1: num = tild(kCCR(VC(id_1)),H(pCC(Sk_id_3,v(jB1)))) in
        {46}let lCCA1_1: num = H(H((VC(id_1),pCA1_1))) in
        {47}let lCCB1_1: num = H(H((VC(id_1),pCB1_1))) in
        {48}let pVCC_2: num = tild(kCCR'(VC(id_1)),H(tild(Sk_id_3,sq(BCK(id_1))))) in
        {49}let lVCC_2: num = H(H((VC(id_1),pVCC_2))) in
        (
            {50}out(c, choice[lpCCA1_1,lpCCB1_1])
        ) | (
            {51}out(c, choice[lpCCB1_1,lpCCA1_1])
        ) | (
            {52}out(c, choice[lCCA1_1,lCCB1_1])
        ) | (
            {53}out(c, choice[lCCB1_1,lCCA1_1])
        ) | (
            {54}out(c, lVCC_2)
        )
    ) | (
        {55}!
        {56}in(c, J_1: num);
        {57}if (not(is_target_voter_1) || ((J_1 ≠ jA1) && (J_1 ≠ jB1))) then
        {58}let hpCC_1: num = H(pCC(Sk_id_3,v(J_1))) in
        {59}let lpCC_1: num = H((hpCC_1,VC(id_1))) in
        {60}let pC_1: num = tild(kCCR(VC(id_1)),H(pCC(Sk_id_3,v(J_1)))) in
        {61}let lCC_1: num = H(H((VC(id_1),pC_1))) in
        {62}let pVCC_3: num = tild(kCCR'(VC(id_1)),H(tild(Sk_id_3,sq(BCK(id_1))))) in
        {63}let lVCC_3: num = H(H((VC(id_1),pVCC_3))) in
        {64}out(c, (lpCC_1,lCC_1,lVCC_3))
    )
) | (
    {65}out(c, pube(sk_EL_1));
    {66}out(c, mergepk(pube(sk_EL_1)));
    {67}let Id: agent_id = idA in
    {68}out(c, pube(ske(Id)));
    {69}let Id_1: agent_id = idB in
    {70}out(c, pube(ske(Id_1)));
    (
        {71}let InitData: bitstring = AliceData(idA) in
        {72}let J1: num = choice[jA1,jB1] in
        {73}let (SVK_id_2: password,VC_id: bitstring,BCK_id: num,VCCid: bitstring,CCid1: bitstring) = GetAliceData(InitData,J1) in
        {74}out(c, VC_id);
        {75}in(c, VCks_id_2: bitstring);
        {76}let KSkey_id_2: symmetric_ekey = KSkey(SVK_id_2) in
        {77}let Sk_id_4: private_ekey = catch-fail(Dec_s(VCks_id_2,KSkey_id_2)) in
        {78}let v_3: bool = not-caught-fail(Sk_id_4) in
        {79}let Sk_id_5: private_ekey = (if v_3 then Sk_id_4 else fail-any) in
        {80}let pk_EL_1: public_ekey = mergepk(pube(sk_EL_1)) in
        {81}let Pk_id: public_ekey = pube(Sk_id_5) in
        {82}let V: num = v(J1) in
        {83}new R: num;
        {84}let E1: bitstring = Enc(pk_EL_1,V,R) in
        {85}let E2: bitstring = pCC(Sk_id_5,v(J1)) in
        {86}let P: bitstring = ZKP(pk_EL_1,Pk_id,VC_id,E1,E2,R,Sk_id_5) in
        {87}out(c, (E1,E2,tild(Sk_id_5,E1),Pk_id,P));
        {88}in(c, Rcv_CC_1: bitstring);
        {89}if (Rcv_CC_1 = CCid1) then
        {90}out(c, pCC(Sk_id_5,sq(BCK_id)));
        {91}in(c, =VCCid)
    ) | (
        {92}let InitData_1: bitstring = AliceData(idB) in
        {93}let J1_1: num = choice[jB1,jA1] in
        {94}let (SVK_id_3: password,VC_id_1: bitstring,BCK_id_1: num,VCCid_1: bitstring,CCid1_1: bitstring) = GetAliceData(InitData_1,J1_1) in
        {95}out(c, VC_id_1);
        {96}in(c, VCks_id_3: bitstring);
        {97}let KSkey_id_3: symmetric_ekey = KSkey(SVK_id_3) in
        {98}let Sk_id_6: private_ekey = catch-fail(Dec_s(VCks_id_3,KSkey_id_3)) in
        {99}let v_4: bool = not-caught-fail(Sk_id_6) in
        {100}let Sk_id_7: private_ekey = (if v_4 then Sk_id_6 else fail-any) in
        {101}let pk_EL_2: public_ekey = mergepk(pube(sk_EL_1)) in
        {102}let Pk_id_1: public_ekey = pube(Sk_id_7) in
        {103}let V_1: num = v(J1_1) in
        {104}new R_1: num;
        {105}let E1_1: bitstring = Enc(pk_EL_2,V_1,R_1) in
        {106}let E2_1: bitstring = pCC(Sk_id_7,v(J1_1)) in
        {107}let P_1: bitstring = ZKP(pk_EL_2,Pk_id_1,VC_id_1,E1_1,E2_1,R_1,Sk_id_7) in
        {108}out(c, (E1_1,E2_1,tild(Sk_id_7,E1_1),Pk_id_1,P_1));
        {109}in(c, Rcv_CC: bitstring);
        {110}if (Rcv_CC = CCid1_1) then
        {111}out(c, pCC(Sk_id_7,sq(BCK_id_1)));
        {112}in(c, =VCCid_1)
    ) | (
        {113}new idC: agent_id;
        {114}out(c, AliceData(idC));
        {115}let id_2: agent_id = idC in
        {116}let is_target_voter_2: bool = false in
        {117}in(c, VCks_id_4: bitstring);
        {118}let SVK_id_4: password = SVK(id_2) in
        {119}let KSkey_id_4: symmetric_ekey = KSkey(SVK_id_4) in
        {120}let Sk_id_8: private_ekey = catch-fail(Dec_s(VCks_id_4,KSkey_id_4)) in
        {121}let v_5: bool = not-caught-fail(Sk_id_8) in
        {122}let Sk_id_9: private_ekey = (if v_5 then Sk_id_8 else fail-any) in
        {123}if is_target_voter_2 then
        (
            {124}let (hpCCA1_2: num,hpCCB1_2: num) = (H(pCC(Sk_id_9,v(jA1))),H(pCC(Sk_id_9,v(jB1)))) in
            {125}let (lpCCA1_2: num,lpCCB1_2: num) = (H((hpCCA1_2,VC(id_2))),H((hpCCB1_2,VC(id_2)))) in
            {126}let pCA1_2: num = tild(kCCR(VC(id_2)),H(pCC(Sk_id_9,v(jA1)))) in
            {127}let pCB1_2: num = tild(kCCR(VC(id_2)),H(pCC(Sk_id_9,v(jB1)))) in
            {128}let lCCA1_2: num = H(H((VC(id_2),pCA1_2))) in
            {129}let lCCB1_2: num = H(H((VC(id_2),pCB1_2))) in
            {130}let pVCC_4: num = tild(kCCR'(VC(id_2)),H(tild(Sk_id_9,sq(BCK(id_2))))) in
            {131}let lVCC_4: num = H(H((VC(id_2),pVCC_4))) in
            (
                {132}out(c, choice[lpCCA1_2,lpCCB1_2])
            ) | (
                {133}out(c, choice[lpCCB1_2,lpCCA1_2])
            ) | (
                {134}out(c, choice[lCCA1_2,lCCB1_2])
            ) | (
                {135}out(c, choice[lCCB1_2,lCCA1_2])
            ) | (
                {136}out(c, lVCC_4)
            )
        ) | (
            {137}!
            {138}in(c, J_2: num);
            {139}if (not(is_target_voter_2) || ((J_2 ≠ jA1) && (J_2 ≠ jB1))) then
            {140}let hpCC_2: num = H(pCC(Sk_id_9,v(J_2))) in
            {141}let lpCC_2: num = H((hpCC_2,VC(id_2))) in
            {142}let pC_2: num = tild(kCCR(VC(id_2)),H(pCC(Sk_id_9,v(J_2)))) in
            {143}let lCC_2: num = H(H((VC(id_2),pC_2))) in
            {144}let pVCC_5: num = tild(kCCR'(VC(id_2)),H(tild(Sk_id_9,sq(BCK(id_2))))) in
            {145}let lVCC_5: num = H(H((VC(id_2),pVCC_5))) in
            {146}out(c, (lpCC_2,lCC_2,lVCC_5))
        )
    ) | (
        {147}let idA_1: agent_id = idA in
        {148}let idB_1: agent_id = idB in
        {149}in(c, (VC_id1: bitstring,B1: bitstring,VCC1: bitstring));
        {150}in(c, (VC_id2: bitstring,B2: bitstring,VCC2: bitstring));
        {151}in(c, (VC_id3: bitstring,B3: bitstring,VCC3: bitstring));
        {152}if (VC_id1 = VC(idA_1)) then
        {153}if (VC_id2 = VC(idB_1)) then
        {154}let pk_EL_3: public_ekey = mergepk(pube(sk_EL_1)) in
        {155}let v_6: bitstring = B1 in
        {156}let v_7: bool = (not-caught-fail(v_6) && success?(1-proj-5-tuple(v_6))) in
        {157}let P_2: bitstring = (if v_7 then 5-proj-5-tuple(v_6) else caught-fail) in
        {158}let Pk_id_2: public_ekey = (if v_7 then 4-proj-5-tuple(v_6) else caught-fail) in
        {159}let EC: num = (if v_7 then 3-proj-5-tuple(v_6) else caught-fail) in
        {160}let E2_2: bitstring = (if v_7 then 2-proj-5-tuple(v_6) else caught-fail) in
        {161}let E1_2: bitstring = (if v_7 then 1-proj-5-tuple(v_6) else caught-fail) in
        {162}let Ok: bool = (if v_7 then catch-fail(VerifP(pk_EL_3,Pk_id_2,VC_id1,E1_2,E2_2,P_2)) else caught-fail) in
        {163}let v_8: bool = not-caught-fail(Ok) in
        {164}let Ok1: bool = (if v_7 then (if v_8 then true else fail-any) else fail-any) in
        {165}let pk_EL_4: public_ekey = mergepk(pube(sk_EL_1)) in
        {166}let v_9: bitstring = B2 in
        {167}let v_10: bool = (not-caught-fail(v_9) && success?(1-proj-5-tuple(v_9))) in
        {168}let P_3: bitstring = (if v_10 then 5-proj-5-tuple(v_9) else caught-fail) in
        {169}let Pk_id_3: public_ekey = (if v_10 then 4-proj-5-tuple(v_9) else caught-fail) in
        {170}let EC_1: num = (if v_10 then 3-proj-5-tuple(v_9) else caught-fail) in
        {171}let E2_3: bitstring = (if v_10 then 2-proj-5-tuple(v_9) else caught-fail) in
        {172}let E1_3: bitstring = (if v_10 then 1-proj-5-tuple(v_9) else caught-fail) in
        {173}let Ok_1: bool = (if v_10 then catch-fail(VerifP(pk_EL_4,Pk_id_3,VC_id2,E1_3,E2_3,P_3)) else caught-fail) in
        {174}let v_11: bool = not-caught-fail(Ok_1) in
        {175}let Ok2: bool = (if v_10 then (if v_11 then true else fail-any) else fail-any) in
        {176}let pk_EL_5: public_ekey = mergepk(pube(sk_EL_1)) in
        {177}let v_12: bitstring = B3 in
        {178}let v_13: bool = (not-caught-fail(v_12) && success?(1-proj-5-tuple(v_12))) in
        {179}let P_4: bitstring = (if v_13 then 5-proj-5-tuple(v_12) else caught-fail) in
        {180}let Pk_id_4: public_ekey = (if v_13 then 4-proj-5-tuple(v_12) else caught-fail) in
        {181}let EC_2: num = (if v_13 then 3-proj-5-tuple(v_12) else caught-fail) in
        {182}let E2_4: bitstring = (if v_13 then 2-proj-5-tuple(v_12) else caught-fail) in
        {183}let E1_4: bitstring = (if v_13 then 1-proj-5-tuple(v_12) else caught-fail) in
        {184}let Ok_2: bool = (if v_13 then catch-fail(VerifP(pk_EL_5,Pk_id_4,VC_id3,E1_4,E2_4,P_4)) else caught-fail) in
        {185}let v_14: bool = not-caught-fail(Ok_2) in
        {186}let Ok3: bool = (if v_13 then (if v_14 then true else fail-any) else fail-any) in
        {187}if ((VC_id1 ≠ VC_id2) && ((VC_id1 ≠ VC_id3) && (VC_id2 ≠ VC_id3))) then
        {188}let (E1_5: bitstring,E2_5: bitstring,EC1: num,Pk_id1: public_ekey,P1: bitstring) = B1 in
        {189}let (E1_6: bitstring,E2_6: bitstring,EC2: num,Pk_id2: public_ekey,P2: bitstring) = B2 in
        {190}let (E1_7: bitstring,E2_7: bitstring,EC3: num,Pk_id3: public_ekey,P3: bitstring) = B3 in
        {191}let L_0: bitstring = (E1_5,E1_6,E1_7) in
        {192}let (E1_8: bitstring,E2_8: bitstring,E3: bitstring) = L_0 in
        (
            {193}out(mix, choice[E1_8,E2_8])
        ) | (
            {194}out(mix, choice[E2_8,E1_8])
        ) | (
            {195}in(mix, m1: bitstring);
            {196}in(mix, m2: bitstring);
            {197}let m3: bitstring = E3 in
            {198}out(c, (PDec(sk_EL_1,m1),PDec(sk_EL_1,m2),PDec(sk_EL_1,m3)));
            {199}out(c, PMix(sk_EL_1,(E1_8,E2_8,E3),(PDec(sk_EL_1,m1),PDec(sk_EL_1,m2),PDec(sk_EL_1,m3))))
        )
    )
)

-- Observational equivalence in biprocess 1 (that is, biprocess 0, with let moved downwards):
(
    {3}in(c, VCks_id: bitstring);
    {1}let id: agent_id = idA in
    {4}let SVK_id: password = SVK(id) in
    {5}let KSkey_id: symmetric_ekey = KSkey(SVK_id) in
    {6}let Sk_id: private_ekey = catch-fail(Dec_s(VCks_id,KSkey_id)) in
    {7}let v_1: bool = not-caught-fail(Sk_id) in
    {8}let Sk_id_1: private_ekey = (if v_1 then Sk_id else fail-any) in
    {2}let is_target_voter: bool = true in
    {9}if is_target_voter then
    (
        {10}let (hpCCA1: num,hpCCB1: num) = (H(pCC(Sk_id_1,v(jA1))),H(pCC(Sk_id_1,v(jB1)))) in
        {11}let (lpCCA1: num,lpCCB1: num) = (H((hpCCA1,VC(id))),H((hpCCB1,VC(id)))) in
        (
            {18}out(c, choice[lpCCA1,lpCCB1])
        ) | (
            {19}out(c, choice[lpCCB1,lpCCA1])
        ) | (
            {13}let pCB1: num = tild(kCCR(VC(id)),H(pCC(Sk_id_1,v(jB1)))) in
            {15}let lCCB1: num = H(H((VC(id),pCB1))) in
            {12}let pCA1: num = tild(kCCR(VC(id)),H(pCC(Sk_id_1,v(jA1)))) in
            {14}let lCCA1: num = H(H((VC(id),pCA1))) in
            (
                {20}out(c, choice[lCCA1,lCCB1])
            ) | (
                {21}out(c, choice[lCCB1,lCCA1])
            ) | (
                {16}let pVCC: num = tild(kCCR'(VC(id)),H(tild(Sk_id_1,sq(BCK(id))))) in
                {17}let lVCC: num = H(H((VC(id),pVCC))) in
                {22}out(c, lVCC)
            )
        )
    ) | (
        {23}!
        {24}in(c, J: num);
        {25}if (not(is_target_voter) || ((J ≠ jA1) && (J ≠ jB1))) then
        {30}let pVCC_1: num = tild(kCCR'(VC(id)),H(tild(Sk_id_1,sq(BCK(id))))) in
        {31}let lVCC_1: num = H(H((VC(id),pVCC_1))) in
        {28}let pC: num = tild(kCCR(VC(id)),H(pCC(Sk_id_1,v(J)))) in
        {29}let lCC: num = H(H((VC(id),pC))) in
        {26}let hpCC: num = H(pCC(Sk_id_1,v(J))) in
        {27}let lpCC: num = H((hpCC,VC(id))) in
        {32}out(c, (lpCC,lCC,lVCC_1))
    )
) | (
    {35}in(c, VCks_id_1: bitstring);
    {33}let id_1: agent_id = idB in
    {36}let SVK_id_1: password = SVK(id_1) in
    {37}let KSkey_id_1: symmetric_ekey = KSkey(SVK_id_1) in
    {38}let Sk_id_2: private_ekey = catch-fail(Dec_s(VCks_id_1,KSkey_id_1)) in
    {39}let v_2: bool = not-caught-fail(Sk_id_2) in
    {40}let Sk_id_3: private_ekey = (if v_2 then Sk_id_2 else fail-any) in
    {34}let is_target_voter_1: bool = true in
    {41}if is_target_voter_1 then
    (
        {42}let (hpCCA1_1: num,hpCCB1_1: num) = (H(pCC(Sk_id_3,v(jA1))),H(pCC(Sk_id_3,v(jB1)))) in
        {43}let (lpCCA1_1: num,lpCCB1_1: num) = (H((hpCCA1_1,VC(id_1))),H((hpCCB1_1,VC(id_1)))) in
        (
            {50}out(c, choice[lpCCA1_1,lpCCB1_1])
        ) | (
            {51}out(c, choice[lpCCB1_1,lpCCA1_1])
        ) | (
            {45}let pCB1_1: num = tild(kCCR(VC(id_1)),H(pCC(Sk_id_3,v(jB1)))) in
            {47}let lCCB1_1: num = H(H((VC(id_1),pCB1_1))) in
            {44}let pCA1_1: num = tild(kCCR(VC(id_1)),H(pCC(Sk_id_3,v(jA1)))) in
            {46}let lCCA1_1: num = H(H((VC(id_1),pCA1_1))) in
            (
                {52}out(c, choice[lCCA1_1,lCCB1_1])
            ) | (
                {53}out(c, choice[lCCB1_1,lCCA1_1])
            ) | (
                {48}let pVCC_2: num = tild(kCCR'(VC(id_1)),H(tild(Sk_id_3,sq(BCK(id_1))))) in
                {49}let lVCC_2: num = H(H((VC(id_1),pVCC_2))) in
                {54}out(c, lVCC_2)
            )
        )
    ) | (
        {55}!
        {56}in(c, J_1: num);
        {57}if (not(is_target_voter_1) || ((J_1 ≠ jA1) && (J_1 ≠ jB1))) then
        {62}let pVCC_3: num = tild(kCCR'(VC(id_1)),H(tild(Sk_id_3,sq(BCK(id_1))))) in
        {63}let lVCC_3: num = H(H((VC(id_1),pVCC_3))) in
        {60}let pC_1: num = tild(kCCR(VC(id_1)),H(pCC(Sk_id_3,v(J_1)))) in
        {61}let lCC_1: num = H(H((VC(id_1),pC_1))) in
        {58}let hpCC_1: num = H(pCC(Sk_id_3,v(J_1))) in
        {59}let lpCC_1: num = H((hpCC_1,VC(id_1))) in
        {64}out(c, (lpCC_1,lCC_1,lVCC_3))
    )
) | (
    {65}out(c, pube(sk_EL_1));
    {66}out(c, mergepk(pube(sk_EL_1)));
    {67}let Id: agent_id = idA in
    {68}out(c, pube(ske(Id)));
    {69}let Id_1: agent_id = idB in
    {70}out(c, pube(ske(Id_1)));
    (
        {72}let J1: num = choice[jA1,jB1] in
        {71}let InitData: bitstring = AliceData(idA) in
        {73}let (SVK_id_2: password,VC_id: bitstring,BCK_id: num,VCCid: bitstring,CCid1: bitstring) = GetAliceData(InitData,J1) in
        {74}out(c, VC_id);
        {75}in(c, VCks_id_2: bitstring);
        {76}let KSkey_id_2: symmetric_ekey = KSkey(SVK_id_2) in
        {77}let Sk_id_4: private_ekey = catch-fail(Dec_s(VCks_id_2,KSkey_id_2)) in
        {78}let v_3: bool = not-caught-fail(Sk_id_4) in
        {79}let Sk_id_5: private_ekey = (if v_3 then Sk_id_4 else fail-any) in
        {83}new R: num;
        {85}let E2: bitstring = pCC(Sk_id_5,v(J1)) in
        {82}let V: num = v(J1) in
        {80}let pk_EL_1: public_ekey = mergepk(pube(sk_EL_1)) in
        {84}let E1: bitstring = Enc(pk_EL_1,V,R) in
        {81}let Pk_id: public_ekey = pube(Sk_id_5) in
        {86}let P: bitstring = ZKP(pk_EL_1,Pk_id,VC_id,E1,E2,R,Sk_id_5) in
        {87}out(c, (E1,E2,tild(Sk_id_5,E1),Pk_id,P));
        {88}in(c, Rcv_CC_1: bitstring);
        {89}if (Rcv_CC_1 = CCid1) then
        {90}out(c, pCC(Sk_id_5,sq(BCK_id)));
        {91}in(c, =VCCid)
    ) | (
        {93}let J1_1: num = choice[jB1,jA1] in
        {92}let InitData_1: bitstring = AliceData(idB) in
        {94}let (SVK_id_3: password,VC_id_1: bitstring,BCK_id_1: num,VCCid_1: bitstring,CCid1_1: bitstring) = GetAliceData(InitData_1,J1_1) in
        {95}out(c, VC_id_1);
        {96}in(c, VCks_id_3: bitstring);
        {97}let KSkey_id_3: symmetric_ekey = KSkey(SVK_id_3) in
        {98}let Sk_id_6: private_ekey = catch-fail(Dec_s(VCks_id_3,KSkey_id_3)) in
        {99}let v_4: bool = not-caught-fail(Sk_id_6) in
        {100}let Sk_id_7: private_ekey = (if v_4 then Sk_id_6 else fail-any) in
        {104}new R_1: num;
        {106}let E2_1: bitstring = pCC(Sk_id_7,v(J1_1)) in
        {103}let V_1: num = v(J1_1) in
        {101}let pk_EL_2: public_ekey = mergepk(pube(sk_EL_1)) in
        {105}let E1_1: bitstring = Enc(pk_EL_2,V_1,R_1) in
        {102}let Pk_id_1: public_ekey = pube(Sk_id_7) in
        {107}let P_1: bitstring = ZKP(pk_EL_2,Pk_id_1,VC_id_1,E1_1,E2_1,R_1,Sk_id_7) in
        {108}out(c, (E1_1,E2_1,tild(Sk_id_7,E1_1),Pk_id_1,P_1));
        {109}in(c, Rcv_CC: bitstring);
        {110}if (Rcv_CC = CCid1_1) then
        {111}out(c, pCC(Sk_id_7,sq(BCK_id_1)));
        {112}in(c, =VCCid_1)
    ) | (
        {113}new idC: agent_id;
        {114}out(c, AliceData(idC));
        {117}in(c, VCks_id_4: bitstring);
        {115}let id_2: agent_id = idC in
        {118}let SVK_id_4: password = SVK(id_2) in
        {119}let KSkey_id_4: symmetric_ekey = KSkey(SVK_id_4) in
        {120}let Sk_id_8: private_ekey = catch-fail(Dec_s(VCks_id_4,KSkey_id_4)) in
        {121}let v_5: bool = not-caught-fail(Sk_id_8) in
        {122}let Sk_id_9: private_ekey = (if v_5 then Sk_id_8 else fail-any) in
        {116}let is_target_voter_2: bool = false in
        {123}if is_target_voter_2 then
        (
            {124}let (hpCCA1_2: num,hpCCB1_2: num) = (H(pCC(Sk_id_9,v(jA1))),H(pCC(Sk_id_9,v(jB1)))) in
            {125}let (lpCCA1_2: num,lpCCB1_2: num) = (H((hpCCA1_2,VC(id_2))),H((hpCCB1_2,VC(id_2)))) in
            (
                {132}out(c, choice[lpCCA1_2,lpCCB1_2])
            ) | (
                {133}out(c, choice[lpCCB1_2,lpCCA1_2])
            ) | (
                {127}let pCB1_2: num = tild(kCCR(VC(id_2)),H(pCC(Sk_id_9,v(jB1)))) in
                {129}let lCCB1_2: num = H(H((VC(id_2),pCB1_2))) in
                {126}let pCA1_2: num = tild(kCCR(VC(id_2)),H(pCC(Sk_id_9,v(jA1)))) in
                {128}let lCCA1_2: num = H(H((VC(id_2),pCA1_2))) in
                (
                    {134}out(c, choice[lCCA1_2,lCCB1_2])
                ) | (
                    {135}out(c, choice[lCCB1_2,lCCA1_2])
                ) | (
                    {130}let pVCC_4: num = tild(kCCR'(VC(id_2)),H(tild(Sk_id_9,sq(BCK(id_2))))) in
                    {131}let lVCC_4: num = H(H((VC(id_2),pVCC_4))) in
                    {136}out(c, lVCC_4)
                )
            )
        ) | (
            {137}!
            {138}in(c, J_2: num);
            {139}if (not(is_target_voter_2) || ((J_2 ≠ jA1) && (J_2 ≠ jB1))) then
            {144}let pVCC_5: num = tild(kCCR'(VC(id_2)),H(tild(Sk_id_9,sq(BCK(id_2))))) in
            {145}let lVCC_5: num = H(H((VC(id_2),pVCC_5))) in
            {142}let pC_2: num = tild(kCCR(VC(id_2)),H(pCC(Sk_id_9,v(J_2)))) in
            {143}let lCC_2: num = H(H((VC(id_2),pC_2))) in
            {140}let hpCC_2: num = H(pCC(Sk_id_9,v(J_2))) in
            {141}let lpCC_2: num = H((hpCC_2,VC(id_2))) in
            {146}out(c, (lpCC_2,lCC_2,lVCC_5))
        )
    ) | (
        {149}in(c, (VC_id1: bitstring,B1: bitstring,VCC1: bitstring));
        {150}in(c, (VC_id2: bitstring,B2: bitstring,VCC2: bitstring));
        {151}in(c, (VC_id3: bitstring,B3: bitstring,VCC3: bitstring));
        {147}let idA_1: agent_id = idA in
        {152}if (VC_id1 = VC(idA_1)) then
        {148}let idB_1: agent_id = idB in
        {153}if (VC_id2 = VC(idB_1)) then
        {155}let v_6: bitstring = B1 in
        {156}let v_7: bool = (not-caught-fail(v_6) && success?(1-proj-5-tuple(v_6))) in
        {157}let P_2: bitstring = (if v_7 then 5-proj-5-tuple(v_6) else caught-fail) in
        {158}let Pk_id_2: public_ekey = (if v_7 then 4-proj-5-tuple(v_6) else caught-fail) in
        {159}let EC: num = (if v_7 then 3-proj-5-tuple(v_6) else caught-fail) in
        {160}let E2_2: bitstring = (if v_7 then 2-proj-5-tuple(v_6) else caught-fail) in
        {161}let E1_2: bitstring = (if v_7 then 1-proj-5-tuple(v_6) else caught-fail) in
        {154}let pk_EL_3: public_ekey = mergepk(pube(sk_EL_1)) in
        {162}let Ok: bool = (if v_7 then catch-fail(VerifP(pk_EL_3,Pk_id_2,VC_id1,E1_2,E2_2,P_2)) else caught-fail) in
        {163}let v_8: bool = not-caught-fail(Ok) in
        {164}let Ok1: bool = (if v_7 then (if v_8 then true else fail-any) else fail-any) in
        {166}let v_9: bitstring = B2 in
        {167}let v_10: bool = (not-caught-fail(v_9) && success?(1-proj-5-tuple(v_9))) in
        {168}let P_3: bitstring = (if v_10 then 5-proj-5-tuple(v_9) else caught-fail) in
        {169}let Pk_id_3: public_ekey = (if v_10 then 4-proj-5-tuple(v_9) else caught-fail) in
        {170}let EC_1: num = (if v_10 then 3-proj-5-tuple(v_9) else caught-fail) in
        {171}let E2_3: bitstring = (if v_10 then 2-proj-5-tuple(v_9) else caught-fail) in
        {172}let E1_3: bitstring = (if v_10 then 1-proj-5-tuple(v_9) else caught-fail) in
        {165}let pk_EL_4: public_ekey = mergepk(pube(sk_EL_1)) in
        {173}let Ok_1: bool = (if v_10 then catch-fail(VerifP(pk_EL_4,Pk_id_3,VC_id2,E1_3,E2_3,P_3)) else caught-fail) in
        {174}let v_11: bool = not-caught-fail(Ok_1) in
        {175}let Ok2: bool = (if v_10 then (if v_11 then true else fail-any) else fail-any) in
        {177}let v_12: bitstring = B3 in
        {178}let v_13: bool = (not-caught-fail(v_12) && success?(1-proj-5-tuple(v_12))) in
        {179}let P_4: bitstring = (if v_13 then 5-proj-5-tuple(v_12) else caught-fail) in
        {180}let Pk_id_4: public_ekey = (if v_13 then 4-proj-5-tuple(v_12) else caught-fail) in
        {181}let EC_2: num = (if v_13 then 3-proj-5-tuple(v_12) else caught-fail) in
        {182}let E2_4: bitstring = (if v_13 then 2-proj-5-tuple(v_12) else caught-fail) in
        {183}let E1_4: bitstring = (if v_13 then 1-proj-5-tuple(v_12) else caught-fail) in
        {176}let pk_EL_5: public_ekey = mergepk(pube(sk_EL_1)) in
        {184}let Ok_2: bool = (if v_13 then catch-fail(VerifP(pk_EL_5,Pk_id_4,VC_id3,E1_4,E2_4,P_4)) else caught-fail) in
        {185}let v_14: bool = not-caught-fail(Ok_2) in
        {186}let Ok3: bool = (if v_13 then (if v_14 then true else fail-any) else fail-any) in
        {187}if ((VC_id1 ≠ VC_id2) && ((VC_id1 ≠ VC_id3) && (VC_id2 ≠ VC_id3))) then
        {188}let (E1_5: bitstring,E2_5: bitstring,EC1: num,Pk_id1: public_ekey,P1: bitstring) = B1 in
        {189}let (E1_6: bitstring,E2_6: bitstring,EC2: num,Pk_id2: public_ekey,P2: bitstring) = B2 in
        {190}let (E1_7: bitstring,E2_7: bitstring,EC3: num,Pk_id3: public_ekey,P3: bitstring) = B3 in
        {191}let L_0: bitstring = (E1_5,E1_6,E1_7) in
        {192}let (E1_8: bitstring,E2_8: bitstring,E3: bitstring) = L_0 in
        (
            {193}out(mix, choice[E1_8,E2_8])
        ) | (
            {194}out(mix, choice[E2_8,E1_8])
        ) | (
            {195}in(mix, m1: bitstring);
            {196}in(mix, m2: bitstring);
            {197}let m3: bitstring = E3 in
            {198}out(c, (PDec(sk_EL_1,m1),PDec(sk_EL_1,m2),PDec(sk_EL_1,m3)));
            {199}out(c, PMix(sk_EL_1,(E1_8,E2_8,E3),(PDec(sk_EL_1,m1),PDec(sk_EL_1,m2),PDec(sk_EL_1,m3))))
        )
    )
)

Translating the process into Horn clauses...
Termination warning: v_15 ≠ v_16 && attacker2(v_17,v_15) && attacker2(v_17,v_16) -> bad
Selecting 0
Termination warning: v_15 ≠ v_16 && attacker2(v_15,v_17) && attacker2(v_16,v_17) -> bad
Selecting 0
Completing...
Termination warning: v_15 ≠ v_16 && attacker2(v_17,v_15) && attacker2(v_17,v_16) -> bad
Selecting 0
Termination warning: v_15 ≠ v_16 && attacker2(v_15,v_17) && attacker2(v_16,v_17) -> bad
Selecting 0
200 rules inserted. Base: 194 rules (40 with conclusion selected). Queue: 189 rules.
400 rules inserted. Base: 382 rules (44 with conclusion selected). Queue: 118 rules.
600 rules inserted. Base: 559 rules (75 with conclusion selected). Queue: 202 rules.
800 rules inserted. Base: 723 rules (80 with conclusion selected). Queue: 195 rules.
1000 rules inserted. Base: 919 rules (80 with conclusion selected). Queue: 164 rules.
1200 rules inserted. Base: 1090 rules (80 with conclusion selected). Queue: 138 rules.
1400 rules inserted. Base: 1184 rules (81 with conclusion selected). Queue: 135 rules.
1600 rules inserted. Base: 1273 rules (84 with conclusion selected). Queue: 86 rules.
1800 rules inserted. Base: 1376 rules (86 with conclusion selected). Queue: 35 rules.
2000 rules inserted. Base: 1460 rules (97 with conclusion selected). Queue: 60 rules.
RESULT Observational equivalence is true.

--------------------------------------------------------------
Verification summary:

Observational equivalence is true.

--------------------------------------------------------------

