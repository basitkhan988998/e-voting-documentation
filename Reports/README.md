# [Auditability Report](Evoting-Auditability-August-2021.pdf)

## What is the content of this document?

Swiss Post has the structure, readability and comprehensibility of the source code and the documentation significantly improved by applying the auditability framework. This report presents the result of the latest test done and the ranking obtained.

You can find more information on the framework itself in this [article](https://www.sieberpartners.com/news/auditiability-of-software).

### Future Work
We explain in [GitLab issue #20](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/20) how we fixed the remaining issues mentioned in the report or how we want to tackle them in future versions of the e-voting system.
