# [Electoral Model](ElectoralModel.md)

## What is the content of this document?

A quickstart and overview of the electoral model supported.

# [Software development process for the Swiss Post voting system](Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)

## What is the content of this document?

This document describes the software development process. Among other things, it provides information on the agile approach and the tools used, shows the quality aspects of software development and gives an overview of software specification for mathematical algorithms.
