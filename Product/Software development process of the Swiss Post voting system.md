# Software development process for the Swiss Post voting system

## Introduction

This document describes the software development process. Among other things, it provides information on the agile approach and the tools used, shows the quality aspects of software development and gives an overview of software specification for mathematical algorithms.

### Overview of the e-voting service

The e-voting service enables eligible voters to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various parties and software components shown on the schematic diagram enable the voting to take place.

![Overview of the e-voting service](../.gitlab/media/developement_process/image1.jpeg)

The canton (through the "Electoral Administrators") sets up the ballot by configuring the e-voting platform ("Voting Server") with voting questions or lists of candidates via the SDM ("Secure Data Manager") before a vote or election and assigns the authorisations for electronic voting. With the Voting Card Print Service and the printer, the voting card (“Voting Card”) will be provided.
The voting or election material, including the voting card, is sent to the voters by post. The voting or election material is provided with individual security codes. This enables the voter to identify his or her voting rights on the e-voting platform ("Voting Client" and "Voting Server") and to cast his or her vote once.

The electronic voting channel is closed the day before the election date. On the Sunday, the Electoral Board can carry out the decoding and counting. The auditors can use the technical tool ("Verifier") to check that the vote has been conducted properly.

## Post Agile Methodology (PAM)

The Post Agile Methodology (PAM) is applied by Software Development. The e-voting team uses the agile PAM approach during development. This is an agile approach adapted to software development at Swiss Post. The method is based largely on Scrum and focuses on the development of products and solutions in an empirical, incremental and iterative process, right up to market maturity.

The Post Agile Methodology is designed to handle projects in a project mode. The agile practices help users to realise the promises of agile methods, such as "more flexibility", "more ownership", "more value for the customer" and "higher motivation in the team", while meeting the needs of a larger organisation with respect to governance and control.

There is no uniform understanding of what is meant by agile practices and agile methods. For the Post Agile Methodology, we therefore use the following definitions:

**Agile values** -- form the foundation and describe the value system behind the agile way of working

**Agile principles** -- are based on the agile values and describe principles for action

**Agile methods** -- give agile practices an overall structure right up to project management

**Agile practices** (techniques) -- are concrete implementations of agile principles

![Graphic Overview Methods and Practices](../.gitlab/media/developement_process/image2.png)

In the Post Agile Methodology, principles and practices are thus combined into a coherent process. Five roles are described. The roles are the sponsor, the product owner, the Scrum master, the development team and the project controller. The role descriptions list the most important tasks, competences and responsibilities (TAR) for each role. The development team is usually made up of several technical roles. For this reason, PAM describes only the additional TARs that each member of the development team receives. The same applies to the project controller, whose TAR describes the additional tasks, competences and responsibilities that he or she receives in an agile approach.

As can be seen in the figure below, the product owner, the development team and the Scrum master form the Scrum team. The sponsor and financial controller work closely with the Scrum team, but are not included. The same applies to supporting roles that are only called upon selectively.

![Graphic overview of roles in the Post Agile Methodology (PAM)](../.gitlab/media/developement_process/image3.png)

The product owner role is responsible for the product and for prioritising the backlog. The Scrum master is also part of the team and has the task of supporting the team in addressing and removing obstacles and helping to apply the agile principles and methods. A sponsor is defined to manage the financial matters and the requirements for the product.

In the next sections, you will find information about the Scrum methodology and ceremonies.

### Scrum

The ceremonies are carried out using the Scrum method. The requirements, specifications and bug fixes are recorded as user stories or tasks in Jira. These stories form the product backlog. The prioritisation of the backlog is done by the product owner.

Development is carried out in defined sprints. A sprint is a defined period of time -- for example, two weeks. The Scrum team defines the user stories to be completed in a sprint. During a sprint, the Scrum team meets daily to discuss questions or obstacles. After a sprint, a sprint review is conducted to check the work on the product. A sprint retrospective is also conducted to address and learn from positive and negative points. The following graphic gives an overview of Scrum.

![Graphic overview of Scrum](../.gitlab/media/developement_process/image4.png)

The next chapter shows the tools used for software development.

## E-voting service toolchain

This chapter on the toolchain gives an overview of the tools used for the development of the e-voting service. The tools used in software development are state of the art and widely used. They allow a high degree of automation for the build and deployment process. The tools used comply with the specifications and standards of the entire software development process at Swiss Post. The e-voting service is developed according to these specifications and standards. The following graphic illustrates the toolchain.

![Graphic of toolchain for e-voting](../.gitlab/media/developement_process/image5.png)

The tools used and the main area of application are listed below.

### Tools

- [Atlassian Bit Bucket](https://bitbucket.org/)

  - Code repository

  - Source code versioning

  - Supports CI/CD

- [Jenkins](https://www.jenkins.io/)

  - Automation server

  - Used for the creation of the software components

  - Used for the automated test

- [JFrog Artifactory](https://jfrog.com/)
  - Artefact repository (Docker images, jars, etc.)

- [Docker](https://www.docker.com/)

  - Software artefacts -- container runtime

  - Used for the e-voting service and for Kubernetes

- [SonarQube](https://www.sonarqube.org/)

  - Code quality

  - Code security

- [Xray](https://jfrog.com/xray/)

  - Continuous security

  - Universal artefact analysis

  - Third-party licence checks

- [Fortify](https://www.microfocus.com/de-de/products/static-code-analysis-sast/overview)

  - Static code analyser

- [Kubernetes](https://kubernetes.io/)

  - Automated container deployment, scaling and management

- [GitLab](https://gitlab.com/)

  - DevOps platform

  - Source code management for publication

The next chapter describes the quality aspects in software development.

## Quality aspects in software development

This chapter deals with the quality aspects of software development. Some quality specifications are prescribed by the PAM (Scrum), others are implemented through the use of software tools and, in addition, there are controls that are carried out by the developer, such as the four-eyes principle.

The source code is first published and then externally audited by a third-party company to ensure traceability and auditability. Furthermore, Swiss Post Ltd carries out quality management and has a quality management system in place. The quality aspects mentioned are described in more detail below.

### Quality management

The process quality is defined by the organisational and product requirements. All processes are documented and reviewed on a regular basis. The process documentation follows company-wide standards.

#### Staff training

One quality aspect is training. The developers and technical staff are trained with regard to internal requirements. This includes PAM training and training on, among other things, the Code of Conduct, safety and development standards.

#### PAM - Scrum

Scrum ensures the quality of changes with a Definition of Ready and a Definition of Done. Every user story, bug or task that represents a change to be implemented, a bug fix or a new function is checked against these definitions.

##### Definition of Ready and Definition of Done

The Definition of Ready (DoR) is defined by the team in advance and applies to all user stories. The Definition of Ready contains criteria that must be fulfilled in order for a user story to be implemented. These criteria include:

- Is the story clearly understandable for everyone involved?

- Have the acceptance criteria been defined?

- Are the acceptance criteria understandable for everyone involved?

- Have possible external dependencies been eliminated?

As soon as a user story passes these tests, it can be implemented.

The same procedure applies to a user story where the implementation is complete. The implementation is checked against the Definition of Done (DoD). The definitions are as follows.

##### Definitions

- **Definition of Ready (DoR)**
  - Mock-ups are defined for new screens or big changes
  - Non-functional requirements are defined. For example, metrics should be defined for verification (e.g. response time)
  - Functional requirements are defined. Complete functional specifications include alternative forms of the expected product formulated in the Jira-issue or referenced in the specification
  - Functional constraints are defined. Pre- and post-constraints are identified
  - Consequences are defined. Any consequences for other projects or components are defined and communicated to the same (e.g. third-party control component)
  - Dependencies are defined. Any dependencies from other project parties are known and fit into our project timeframe (e.g. Verifier)
  - Task specificity. Specificities are identified (e.g. cryptography)
  - Acceptance criteria are defined. Definition of what must be fulfilled to have the story completed and verified appropriately (i.e. Test Management involvement is required).
  - The task is estimated. The story (including subtasks) is estimated during the sprint planning at the latest
- **Definition of Done (DoD)**
  - Algorithms aligned. Our code follows the algorithms\' specification precisely. Comments in the code justify deviations -- which have to be minor and accepted by the product owner

  - Terminology aligned. Class, variable, method, and argument names align closely with the specification and the naming conventions

  - Coding style aligned. We strive for a clear, concise, and consistent coding style across the entire codebase. To this end, we document best practices and peer-review our code extensively
  - Architecture aligned. We carefully evaluate the risks and benefits of using third-party libraries and frameworks. In cryptographically sensitive code, we limit the usage of external dependencies to a strict minimum
  - Auditable. We acknowledge that a wide range of experts is going to scrutinise our code. Therefore, we make our code as understandable as possible: we structure our code systematically, keep our methods small, define clear interfaces, disentangle our components, and eliminate duplication and domain-specific jargon
  - Robust. Our algorithms shall be robust by design; we validate inputs extensively and pay special attention to edge cases
  - Tested. We test our code systematically and broadly. When writing tests, we keep in mind that parts of the system might be malicious. We verify security-sensitive operations and cryptographic code even more extensively
  - Tool-checked. Our code adheres to coding best practices and successfully passes code quality tools (Sonar, Fortify, X-Ray)
  - Peer-reviewed. At least one architect and one developer approve each code modification. Furthermore, a cryptography expert must review cryptographically-sensitive code
  - Sensibly commented. Our comments in the code are clear, correct, and facilitate the understanding of the underlying code. We eliminate superfluous comments or those that contain person-identifiable information.

###### Retrospective

Another quality aspect is the retrospective ceremonies. The retrospective enables the Scrum team to improve continuously from sprint to sprint. The definition according to the Scrum manual is as follows: "The purpose of the sprint retrospective is to plan ways to improve quality and effectiveness. The Scrum team examines how the last sprint went in terms of people, interactions, processes, tools and their definition of done."

#### The four-eyes principle and the segregation of duties

For software development, quality control is implemented according to the dual control principle. Every code change is checked by other developers and/or architects. The checks take place during the commits to the development environment. This ensures that every change is checked and released before synchronisation.

Another quality and security control is the segregation of duties between the development team, test management and the infrastructure. Developers do not have access to the test and production system environments. All changes are checked before they are synchronised with these environments.

#### Source code quality and auditability

This chapter gives an overview of the software tools used and of auditability for analysing and ensuring source code quality.

##### Software tools

The quality of the source code is ensured through the use of various software tools. The tools and some of their features are listed below.

- SonarQube software

  - Sonar is used to check the source code, the coding against a defined set of rules. The set of rules is available in profiles. The profile used is called "Sonar Way". At the end of each sprint, a report is created and evaluated in the sprint review.

  - The source code is publicly available and so is the "Sonar Way" profile that is used. This allows independent inspections with the same set of rules of the source code.

  - Sonar results are evaluated based on classification and severity.

  - Identified issues are resolved.

- Fortify software

  - Fortify is used to check application security

  - The results are evaluated according to the classification and severity.

  - Identified issues are resolved.

- JFrog Xray software

  - JFrog Xray is used to check continuous safety and universal artefact analysis.

  - The results are evaluated according to the classification and severity.

  - Identified issues are resolved.

An overview of the analyses carried out with the above-mentioned software tools is published regularly for the community. The overview provides information on software quality. An example can be found at the following link.
Link: [Example -- overview of software quality](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/README.md##code-quality)

###### Source code auditability

One requirement from the e-voting regulation is: "The source code must be created and documented according to best practices". This concept is known as auditability. To ensure auditability, the source code is audited by external firms. The companies SIG [(](https://www.softwareimprovementgroup.com/)<https://www.softwareimprovementgroup.com/>) and S&P [(https://www.sieberpartners.com/)](https://www.sieberpartners.com/) have a model in place for this purpose. The source code, test artefacts and documents to be published are analysed and checked according to this model. The results of the audit are listed in an auditability report. The auditability report is published after the tests have been carried out.

#### Testing

Testing of the e-voting service is carried out by the development team and by a dedicated test team. A test concept defines the tests to be carried out. Care is taken to ensure that the test concept can be implemented pragmatically. The defined test cases of the different test types cover the non-functional, the functional and security-specific requirements of the e-voting system. In addition to the manual tests, automated tests are also carried out. The development team performs unit tests, integration tests and end-to-end tests. Further information on the tests is listed in the test concept. The test concept is one of the published documents.

#### Reproducible build and deployment

The reproducible build is understood to be a process for deterministic compilation of the source code. The reproducible build is important for cooperation with the community. The public can use it to check and be sure that the published version is the same as the one created. For this purpose, hash values generated during the compilation are compared. To make a reproducible build possible, it is ensured during development that no non-determinism elements are used (e.g. time stamps in a manifest file).

More information on reproducible builds can be found here: <https://reproducible-builds.org/>

##### Trusted build and deployment

Trusted build and deployment is an organisational approach that describes procedural and organisational measures to prove that the code running on the production system corresponds to the published source code. To this end, a traceable method is applied for making the system available from the source code through to its installation in production (build and deployment).

For more information on trusted build, see the document "Trusted build -- e-voting service".

#### Transparent development -- source code disclosure

Transparent development is Swiss Post's approach to software development for the e-voting service. The source code is published as part of a community programme. The aim of the community programme is to make access easy for independent experts, to improve the system on a continual basis and to enable dialogue between experts and the Swiss Post e-voting team. It also serves to provide transparency and insight into the functions of the e-voting service. Further information can be found here: <https://evoting-community.post.ch/>

The transparent development approach supports disclosure on the following points:

- Software increments
    Software increments are published in addition to the releases. The software increments are made available on a designated environment (branch).

- One commit per feature (in the software increments)
    The commits represent the functional changes to the source code. There is one commit per feature for easier understanding of the changes in the source code.

- Contributions from the community
    Suggestions for changes (e.g. pull requests) from the community are received and reviewed, and if accepted, integrated into the e-voting source code. This means that changes or improvements from the community can be included in the source code.

## Software specification and implementation of mathematical algorithms

The e-voting service contains complex mathematical algorithms. The algorithms are specified with LaTeX documentation software. With LaTeX, complex mathematical notations can be described very precisely. The use of the software allows for easy testing and implementation, which is a key element for the auditability of the system. Further information on LaTeX can be found here: <https://www.latex-project.org/>.
