# Changelog

## Release 0.8.3.0 (2021-09-01)

### Added (7)

- [Symbolic proofs](Symbolic-models)
- [ModSec & CRS Tuning process](Operations/ModSecurity-CRS-Tuning-Concept.md)
- [Verifier specification](https://gitlab.com/swisspost-evoting/verifier)
- [SDM Hardening guidlines](Operations/Recommendation_Safety_Measures_SDM.md)
- [ABOUT](ABOUT.md)
- [Auditability report](Reports/Evoting-Auditability-August-2021.pdf)
- [E-voting sourcecode](https://gitlab.com/swisspost-evoting/e-voting/e-voting)

### Fixed (4)

- fixed smaller typos and errors
- improved project structure and wording
- improved README
- improved markdown compliance according to markdownlint

---

## Release 0.8.2.0 (2021-07-19)

### Added (4 Changes)

- [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- [E-voting Architecture documentation of the Swiss Post Voting System](/System/SwissPost_Voting_System_architecture_document.pdf)
- [Test Concept of the Swiss Post Voting System](/Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)

---

## Release 0.8.1.0 (2021-06-25)

### Fixed (6 Changes)

- minor structural and wording improvements
- fixed smaller typos and errors
- markdown compliance according to markdownlint
- new version of the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
- new version of the [protocol](/Protocol)
- new version of the [system specification](/System/System_Specification.pdf)

---

## Release 0.8.0.0 (2021-05-04)

### Added (5 Changes)

- new directory [System](/System) in /Documentation
  - [Readme](/System/README.md) as accompanying document for the System specifications
    - [System specification](/System/System_Specification.pdf)
- new file [ElectoralModel](/Product/ElectoralModel.md)
- added a changelog-section for the crypto-primitives in the [crypto-primitives readme](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/README.md)

### Fixed (5 Changes)

- structural and wording improvement for the [/Documentation/README.md](/README.md)
- structural and wording improvement for the [overviewfile](Product/Overview.md)
- structural and wording improvement for the [reportingfile](/REPORTING.md)
- new version of the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
- new version of the [protocol](/Protocol)

---

## Release 0.7.0.0 (2021-03-23)

### Added (7 changes)

- new repository [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
  - [README.md](https://gitlab.com/swisspost-evoting/crypto-primitives/readme.md) as accompanying document for the crypto-primitives
    - [Specification](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/cryptographic_primitives_specification.pdf) of the crypto-primitives
- new directory [src](https://gitlab.com/swisspost-evoting/crypto-primitives/src) in /crypto-primitives
  - [Crypto-primitives sourcecode](https://gitlab.com/swisspost-evoting/crypto-primitives/src)
- new directory [product](Product)
  - new File [overview](Product/Overview.md)

### Fixed (4 changes)

- renamed 'Protocol definition' to [/Documentation/Protocol](Protocol) for user-friendliness
- wording improvement for the [/Documentation/Protocol/README.md](/Protocol/README.md)
- Changelog-section in README of [/Documentation](/README.md)
- Overview-section in README of [/Documentation](/README.md)
