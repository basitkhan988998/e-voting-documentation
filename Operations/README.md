# [Infrastructure whitepaper of the Swiss Post voting system](Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)

## What is the content of this document?

The infrastructure whitepaper describes the e-voting infrastructure with all implemented security aspects. This includes information on the data centers, the structure and use of the infrastructure and databases. The various security measures are also described.

# [ModSecurity-CRS-Tuning concept](ModSecurity-CRS-Tuning-Concept.md)

## What is the content of this document?

This document describes the procedure for tuning the reverse proxies.

# [Recommendation safety measures](Recommendation_Safety_Measures_SDM.md)

## What is the content of this document?

Description of the recommended safety measures to prevent manipulation and interference with the voting process.
