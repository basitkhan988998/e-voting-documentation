# Infrastructure whitepaper of the Swiss Post voting system

## Introduction

The infrastructure whitepaper describes the e-voting infrastructure with all implemented security aspects. This includes information on the data centers, the structure and use of the infrastructure and databases. The various security measures are also described.

### Overview of the e-voting service

The e-voting service enables eligible voters ("Voters") to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various parties and software components shown on the schematic diagram below enable the voting to take place.

![Overview e-voting service](../.gitlab/media/whitepaper/image2.jpeg)

The canton (through the “Electoral Administrators”) sets up the ballot by configuring the e-voting platform (“Voting Server”) with voting questions or lists of candidates via the SDM (“Secure Data Manager”) before a vote or election and assigns the authorisations for electronic voting. The Voting Card Print Service and the printer provide voting documents, including the voting cards (“Voting Card”).

The voting or election material, including the voting card, is sent to the voters by post. The voting or election material is provided with individual security codes. This enables the voter to identify his or her voting rights on the e-voting platform ("Voting Client" and "Voting Server") and to cast his or her vote once.

The electronic voting channel is closed the day before the election date. On Sunday, the Electoral Board can carry out the decoding and counting. The auditors can use the technical tool ("Verifier") to check that the vote has been conducted properly.

## IT infrastructure

### Data centers and business continuity

Swiss Post has two geographically separate data centers. The infrastructure is built to be disaster-proof, and the following features distinguish our data centers:

Features

- FINMA-compliant, TÜV "Dual Site Level 3"-certified

- The operator is ISO 27001- and ISO 22301-certified

- Complete redundancies of critical supply systems

- No single point of failure

- Strongly authenticated access control

- Uninterruptible power supply

All e-voting systems are redundantly located in both data centers. If the primary data center fails, the other data center takes over the services. The following graphic illustrates the site redundancy:

![Graphic of the data centers (DC)](../.gitlab/media/whitepaper/image3.jpeg)

In addition to site redundancy, corresponding room redundancy is guaranteed within a data center. This ensures business continuity at all times. The following graphic illustrates the room redundancy:

![Graphic of the business continuity at the data centers](../.gitlab/media/whitepaper/image4.jpeg)

Key:

- DC: data center

#### E-voting infrastructure

The e-voting infrastructure is set up for use within the cantons. Each canton has its own e-voting environment, which is logically completely separate from the environments of the other cantons. The following graphic shows the structure:

![Graphic of the e-voting infrastructure](../.gitlab/media/whitepaper/image5.jpeg)

Key:

- FEV: e-voting service front end

- BEV: e-voting service back end

- RP: reverse proxy

- CCn: control component

- DB: database

- ODA: Oracle database application

Only the control components are used together. Since these are used for the computational control of electoral levies, cantonal separation is not necessary. The Administration Portal (Admin Portal) is a browser-based tool for the administration and configuration of the canton-specific aspects.

In addition to the cantonal separation, there is a dedicated access layer (reverse proxy infrastructure) that has been set up for voting citizens and cantonal administrators. In addition, various demo, test and integration environments are available. The access layer and the test, integration and productive environments are described in detail below.

##### Access layer

The access layer manages access to the e-voting infrastructure. In addition to the cantonal separation, there is a dedicated access layer (reverse proxy infrastructure) that has been set up for voting citizens and cantonal administrators. Swiss Post's reverse proxies run on physical hardware. There are two physical productive servers per data center, a total of four servers for the e-voting environment. The reverse proxy functionality is ensured per server by several instances of the reverse proxy software. For each hardware server, there is one voter reverse proxy, one voter-SDM reverse proxy and one admin reverse proxy per canton. The instances are logically separated from each other. They run as different users with different certificates and separate IP addresses on specially hardened servers under a high-security operating system.

An open-source security framework is used on the reverse proxies. This is a module that provides protection against attacks. The module is operated with two different sets of rules. The voting reverse proxy must be accessible to worldwide clients. Therefore, access cannot be authenticated by means of a client certificate. For this reason, an additional customised set of rules was developed for the voter reverse proxy, which allows only a short, predefined list of accesses to the server.

The e-voting set-up per canton comprises two separate parts: the public voter part and the admin part. The two parts are completely separate; cross-connections are not permitted and are made impossible by firewalls. The admin part is used to set up an election with the SDM (Secure Data Manager). The election is set up via a specially secured channel on the voter application. The voter part is used for the actual ballot for the citizens to vote.

###### Access for the cantons

There are two different access options for the cantons. If the canton has its own infrastructure and its own portal, voters from the canton in question can use it to cast their ballot. Cantons without their own portal can use the Swiss Post voter portal for their voters. The following diagram visualises the access options:

![Graphic of the access for the cantons](../.gitlab/media/whitepaper/image6.jpeg)

In the graphic, the preferred access for the voter and for the administrator is depicted by a dark pink arrow.

###### Integration and production cluster

The integration and productive systems are set up within a cluster. The cluster is in turn based on virtualised servers. With the use of this technology, various factors can be ensured:

- The [Kubernetes](https://kubernetes.io/de/docs/home/) Cluster is an open-source system for automating the deployment, scaling and management of container applications in a highly available infrastructure.

- By using container technology and deployment on the fly, high availability can be ensured and downtime reduced.

- Deployment on the fly plays an essential role in scalability. Cantons can be connected quickly and automatically.

- Resource distribution and resource utilisation can thus be optimally controlled for the various loads.

- If a worker node fails, the load is automatically distributed to the remaining nodes, thus ensuring that the application remains available.

The following diagram shows a schematic overview of the structure:

![Graphic of the integration and production cluster](../.gitlab/media/whitepaper/image7.jpeg)

Security is an essential factor for the e-voting infrastructure. Firewall demarcations within the cluster, the worker nodes and the cantonal instances are ensured by automated Iptables configurations. The use of special forensic software further increases security and is thus an essential part of the security concept. Communication outside and inside the cluster takes place exclusively via TLS (encrypted connection paths).

###### Cluster structure for a canton

Several worker nodes are provided for each canton. Each worker node is a virtualised server based on VMWare. The scaling of the individual instances takes place according to the load or through the set CPU / memory of the lower and upper limits. If the upper limits are reached, a new instance is automatically started to distribute the load as efficiently as possible. If the cluster management determines that the load is again within the specified values, instances that are no longer required are shut down again. For each canton, two instances of the voter front and back end and a message service cluster consisting of three RabbitMQ instances are created as a basic set-up. All accesses to the cluster that do not comply with the HTTPS standard are routed via several proxy controllers.

At a minimum, the system shown in the following diagram is initiated for each canton:

![Graphic of the cluster structure for a canton](../.gitlab/media/whitepaper/image8.jpeg)

Key:

- SNI = server name indentification

- POD = one Docker application instance

- NODE = virtual machine

- Namespaces = namespaces

- RabbitMQ= messaging service

Further information on Kubernetes and Rancher:

- [Kubernetes](https://kubernetes.io/de/docs/home/) homepage

- [Rancher](https://rancher.com/docs/rancher/v2.x/en/overview/architecture/) architecture

Each cantonal e-voting instance is managed as a Rancher project with its own unique Kubernetes namespace. Within the cluster, the individual projects are shielded from each other and from outside to inside by firewall rules. This ensures that all cantons are shielded from each other and that access is possible only within each canton's own Rancher cluster project.

#### Database infrastructure

The e-voting database infrastructure consists of three productive and two integrative, dedicated systems. The ballot box is encrypted and signed on the database. The data must always be consistent and no data may be lost at any time. Therefore, the systems have "triple mirroring" and "zero data loss" to ensure consistency and loss of data.

![Graphic of the database infrastructure](../.gitlab/media/whitepaper/image9.jpeg)

## E-voting security

Swiss Post operates an e-voting system that guarantees individual and universal verifiability in accordance with [the applicable VEleS (Ordinance of the Federal Chancellery on Electronic Voting, SR 161.116](https://www.fedlex.admin.ch/eli/cc/2013/859/en)). Swiss Post ensures the transport of the encrypted ballot papers and the encrypted ballot box. This transport is secured on the Swiss Post systems by additional security measures. The following diagram clearly shows the security-relevant aspects that have been implemented for Swiss Post's e-voting solution:

![Graphic of e-voting security](../.gitlab/media/whitepaper/image10.jpeg)

Key:

- SSL= Secure Sockets Layer (SSL), network protocol

- Voter front end = web-based interface, execution of the vote

- Voter back end = Voting Server

- Admin = Admin Portal\
    The administration portal (Admin Portal front end) is a browser-based tool for the administration and configuration of canton-specific aspects.

-

- SDM = Secure Data Manager (SDM)\
    Locally installed program for uploading, editing and configuring ballots

- RabbitMQ= messaging service

### Safety measures

The various safety measures are described in the coming chapters.

#### Individual and universal verifiability

Individual and universal verifiability are security mechanisms for verifying the results of a ballot. Individual verifiability means that voters compare a verification code that they receive with their voting documents to the verification code displayed online when they go to the ballot box. This ensures that the vote was transmitted correctly.

Universal verifiability is used to check whether votes have been manipulated. This allows electoral authorities to conduct independent checks and audits of a ballot. The diagram below illustrates this process:

![Graphic of individual and universal verifiability](../.gitlab/media/whitepaper/image11.jpeg)

The voting protocol used for this purpose enables verifiability. More detailed information can be found in the document [Swiss Post Voting Protocol Computational proof.pdf](https://gitlab.com/swisspost-evoting/documentation/-/tree/master/Protocol).

##### Control components

The control components are an essential part of the security measures of the e-voting service. Among other things, they are used to create and validate the codes for the voting cards. The control components are designed with fourfold redundancy and, to further increase the security against attacks, they run on four different operating systems and are operated by four different operating teams.

The control components are subject to separate security. In the trust model of universal verifiability, only the control components in their combination are trusted. In addition to the security elements of the e-voting platform that have already been mentioned, additional precautions have been taken to increase security. These support the trust model.

The control components are operated independently of each other so that a possible successful attack on one component cannot affect other components. This ensures that the trustworthiness of a group of control components remains guaranteed.

The operation and monitoring of the control components are the responsibility of different persons. Certain hardware components and the operating systems of the control components differ. The control components are connected to different networks. They are accessible only to persons who are responsible for the operation and monitoring of a specific control component. All access attempts are detected and reported to the person responsible for the corresponding control components.

All control components are based on dedicated and disaster-proof hardware. Each control component group (also called CC group) has its own hardware. Furthermore, the four CC groups are based on different operating systems in order to do justice to the approach of diversity. The operating systems used must meet Swiss Post IT's minimum requirements and are supplemented by additional security software when they are put into operation.

The use of the four control component groups aims to ensure that the cryptographic operations are checked by several systems that use different technology (e.g. different operating systems). Firstly, operations are performed in parallel, and the final result is valid only if all four results are identical. Secondly, operations such as mixing are performed sequentially, and the final result is valid only if all four partial results are correct.

![Graphic of the control components](../.gitlab/media/whitepaper/image12.jpeg)

The control components consist exclusively of physical servers (bare-metal approach) and must meet Swiss Post IT's minimum security standard. Further security measures are then taken in the course of configuring the servers. Furthermore, the control components differ physically from each other. This means that different server models with different processor architectures are in use.

Each control component is physically present in both data centers. The two components placed in DC1 and DC2 are connected by means of redundant load balancers. These load balancers primarily serve to ensure high availability. If a control component in one data center fails, the other component located in the other data center takes over the service. Even in the event of a total failure of a data center, this concept ensures seamless operation. Furthermore, it is ensured that no control component of the same or a foreign group is physically installed in the same rack, and the racks within the high-security data center are also locked.

For the e-voting control components, a separate area was set up in a designated secure zone at Swiss Post. This area in turn contains four different zones (1--4), which are separated / protected from each other by means of a firewall. The e-voting area, in turn, is separated/protected from the other areas located in the superordinate zone by means of a firewall. Each additional zone is in turn separated by a firewall.

###### Access layer / reverse proxies

A large part of the security measures focuses on the access layer with its reverse proxies. This is described in chapter 3.2.1 Access layer. Essentially, it is about enforcing mandatory access control at both operating system and application level.

###### Firewalls, zones and areas

The zone and area concept separates the servers from each other in terms of network technology. This means that the access layer, voter front end, Voter Portal, Admin Portal, admin back end and database are in different networks and cannot see each other. Each zone and area is protected by firewalls. The connection is made by means of regulated and defined firewall rules. The firewall rules define which traffic is allowed and which is prohibited through a firewall. The access layer is physically separated from the rest of the e-voting infrastructure.

###### SDM access / sTunnel

The SDM (Secure Data Manager), which is used to set up the ballot and collect the election results, connects to the admin reverse proxy and to the voter SDM reverse proxy. There is a certificate on the SDM. This is validated by the reverse proxies and used for authentication. This means that only this client can call up the various Voting Admin URLs. The SDM client is always used based on the multi-eye principle and is stored securely when not in use.

The SDM does not offer the possibility to access HTTPS resources directly (e-voting AP/VP). sTunnel or a similar tool must be installed on the notebooks at the cantons and by anyone who wants to use the SDM "securely" with SSL. The sTunnel creates a local HTTP port and then forwards everything to the external HTTPS port.

###### High-security operating system

The infrastructure is built on an open-source high-security operating system that supports the access control mechanism. All servers, except those of the control components (see chapter 3.1.2), for the e-voting platform (reverse proxies and the e-voting servers) use this operating system.

This high-security operating system means that an e-voting server process runs in its own context and is completely encapsulated. This means that the process can access only the designated resources of the system. All other resources are not available to it (mandatory access control).

###### Firewall (Iptable)

In addition to Swiss Post's own physical network firewalls, firewall software is also used at operating system level on the entire e-voting platform in order to provide additional access protection. Only access to the necessary system management ports and from defined web server IP addresses is permitted.

###### Mutual authentication at SSL/TLS level

The various servers communicate in encrypted form. They always authenticate each other by means of certificates. This ensures complete end-to-end encryption (from the recording to the counting of votes) for e-voting.

###### Integrity monitoring

Swiss Post uses an open-source solution for e-voting for integrity monitoring of the operating system and as an IDS (intrusion detection system) on the entire platform.

###### JS response check

The reverse proxy validates the JavaScript files that are sent to a client (voter). In other words, the reverse proxy checks the hashes of the files in the HTTPS response body, which are delivered by the back end systems. If there is a deviation from the configured hash value, it refuses to deliver a possibly manipulated file. This is a control measure that ensures the correct JavaScript-based encryption of the ballots on the client.

###### Air gap principle

The canton operates several workstations (clients) with the SDM software. These are needed for the execution and the verification of a ballot. A fundamental distinction is made here between online and offline devices. The offline device does not have access to a network at any time. Data is transmitted exclusively via encrypted data carriers. The following process visualises the air gap principle for constituting a ballot. The devices with a red square are so-called offline workstations. Data is not copied via a network connection, but exclusively via a medium provided for this purpose, such as a USB stick.

![Graphic of the air gap principle](../.gitlab/media/whitepaper/image13.jpeg)

###### Four-eyes principle

The four-eyes principle controls administrative access to the complete e-voting infrastructure. If Swiss Post system administrators want to access an e-voting component, they need a token number, which they receive from another person from another department after identity and justification checks. This token number is valid only once and expires as soon as the administrator logs off again.

###### E-voting monitoring

The infrastructure components are monitored according to a standardised and ISO-certified process. Alarms are carried out according to defined threshold values via SMS and/or e-mail alerts.

In addition to this monitoring, a so-called voter monitoring was set up for e-voting. The e-voting application generates specific logs with events that can be assigned to individual phases in the voting process. Thus, received and completely anonymised votes can be observed, searched, filtered, statistically analysed and graphically evaluated in real time during a ballot. This monitoring primarily serves to control the orderly process of electronic voting. Critical conditions or anomalies during a ballot trigger an alert that is transmitted by SMS to the defined offices.

###### E-voting deployment process

The deployment process describes how a new release is installed on the e-voting platform. Please note that the integrity of the created release is checked by means of a checksum and that the deployment is possible only with the principle of dual control. Each deployment is recorded in Swiss Post\'s change management tool, tracked and approved after completion of the deployment. Each release is created and published via the trusted build. In the trusted build process, the sources from GitLab, which are publicly accessible, are used. In a further step, it is checked whether these sources correspond to those in Swiss Post IT source management. This serves to prove that the sources in GitLab have not been falsified. If this is not the case, the artifacts are created and then the Docker images are created in turn from them. All these processes are monitored by an independent commission. Further information on the trusted build can be found in the document "E-voting service trusted build".
