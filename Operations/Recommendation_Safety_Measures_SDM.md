# Recommendation safety measures (SDM hardening guidelines)

## Introduction

Swiss Post provides the cantons with an electronic voting channel for votes and elections, in addition to postal and in-person voting.
The administration of the persons who are entitled to vote, the content-related preparatory work for the ballot (questions, lists of candidates) and the consolidation and evaluation of the results remain the responsibility of the cantons. The cantons operate part of the e-voting systems within their infrastructure (decentralised solution variant). They hold the voting registers and are responsible for the secure printing of the voting cards, including the personal authentication and verification codes.

Several PCs with SDM (Secure Data Manager) installed are required at the cantonal offices responsible for election preparation and evaluation. Although the secure operation of these PCs is the responsibility of the cantons, they must be taken into account in the security arrangements of the e-voting platform. This document contains a catalogue of recommendations for the secure operation of SDM PCs as a supplement to the existing security concepts for the client infrastructure.

**In principle, the laptops and storage media may not be made publicly accessible and only the required components of the e-voting solution must be installed on these systems.**

## Protection goals

- Correct execution of the vote/election (conformity with the law)
- Secrecy of the physical identities of the voters (confidentiality)
- Secrecy of the individual votes (confidentiality)
- Correctness of the results (integrity)
- Proof that no manipulation or other interference has taken place (verifiability)

## Threats (generic catalogue of threats from BSI Germany)

- Spying on information / espionage
- Disclosure of sensitive information
- Manipulation of hardware or software
- Unauthorised intrusion into IT systems
- Software vulnerabilities or errors
- Unauthorised use or administration of devices and systems
- Malware

## Procedure

The procedure is described in detail in the relevant Operating and/or User Guide. It essentially comprises the following steps:

![SDM steps](../.gitlab/media/safety_measures/sdm_steps.PNG)

Four PCs are required for administration at the canton:

- Pre-processing SDM
- Online SDM
- Post-processing SDM
- Verifier

The Online SDM is used as a “bridge” for transferring files between the SwissPost servers and the SDM PCs.
The two Processing SDMs are used for processing classified data and, as a rule, are operated in isolation from any kind of network.
The Verifier PC is used to check the reconciliation results for correctness.
For simplicity, the Pre- and Post-processing SDMs and Verifier PCs are referred to as Offline PCs.
For the transfer of data between the Online and Offline PCs, special USB sticks are used.

## Catalogue of generic security measures

These generic security measures for the secure operation of SDM PCs are recommendations and are not binding. They can supplement the existing security concepts for the client infrastructure in the cantons where necessary and appropriate.
It must be checked in each case whether the measures are compatible with the existing configurations on the clients.
For the conception and, if necessary, implementation, the involvement of persons with the appropriate knowledge is recommended.

## Principles for the use of cryptographic procedures

The certificates used shall only be applied following the key usage defined in the certificate. This is mandatory for certificates with the associated asymmetric key, which is used to ensure non-repudiation (digital signature).
Only algorithms whose specifications are published publicly and whose security is confirmed by experts should be used. Algorithms that are described or referenced in international standards are to be preferred to others.
The use of self-signed certificates (X.509) is prohibited.

## Generic security measures – Online PC

The Online PC establishes communication relations with the remote servers at SwissPost. For this reason, it requires a network connection with Internet access. In addition, files are copied to the device via USB and/or SD cards. The USB sticks and/or SD cards used for this purpose are also used on devices that do not belong to the e-voting platform. The threats and the number of possible risk scenarios are therefore considerably higher than for the isolated SDM and Verifier PCs.

### Technical measures

- Block booting from external devices
- Activate BitLocker (with TPM) and encrypt hard disk (additional PIN authentication if necessary)- Activate Secure Boot
- Block access to BIOS/UEFI for user (password)
- Hardening OS
  - Basis CIS Benchmark (www.cisecurity.org)
  - Disable unneeded interfaces (if not already disabled in BIOS)
  - Block execution of USB sticks (.exe and scripts)
  - Disable unneeded devices (e.g. CD drive, smart card, camera, microphone)
  - Disable unneeded services
  - Disable Windows + R (Run)
  - Activate OS Update (message when patches are available)
  - Do not allow Java updates
  - Disable / block mobile hotspot
  - Ensure that NTFS is used on all volumes
  - Enable Windows Firewall (no inbound traffic for all profiles, outbound whitelist approach)
  - Whitelist-allowed external devices (device driver installation), do not allow others
  - Enable UAC (User Account Control)
  - Enable DEP (Data Execution Prevention)
  - Prevent ICMP tunnels
  - Prevent Teredo tunneling
  - Disable IPv6
  - Prevent adding printers (incl. IPP)
  - Activate Enhanced Protected Mode (EPM) for browsers
  - Enable auditing (define event logs large enough, min. 200MB)
  - Configure Account Logon audit policy
  - Configure Account Management audit policy
  - Configure Logon/Logoff audit policy
  - Configure Policy Change audit policy
  - Configure Privilege Use audit policy
  - Prevent the creation or use of Microsoft Accounts
  - Automatically lock PC when inactive (possibly via screen saver)
  - Prevent sending of unencrypted passwords to SMB shares
  - Configure security settings when connecting to a domain
  - Block remote registry access
  - Prevent RDP access
  - Prevent automatic administrative logon to Recovery Console
- Configure Windows Defender (Windows Defender Advanced Threat Protection if applicable)
- Defender: Turn off cloud-based protection and automatic submission of samples
- Implement application whitelisting
- Implement secure deletion of files and directories
- Synchronize time/date with trusted time servers (e.g. ntp.pool.org)
- Implement appropriate authorization concept (least privilege principle)
  - Restrict access to the PC to authenticated users and administrators
  - Deactivate guest accounts
  - Users with standard rights (users)
  - Complex passwords
- Check DNS settings (Auto or fixed DNS?) and proxy settings (Auto detect?)

## Generic security measures – Offline PC

### Technical measures – Offline PC

- Block booting from external devices
- Activate BitLocker (with TPM) and encrypt hard disk (additional PIN authentication if necessary)
- Activate Secure Boot
- Block access to BIOS/UEFI for user (password)
- Hardening OS
  - Basis CIS Benchmark (www.cisecurity.org)
  - Deactivate all interfaces (except USB)
  - Disable unnecessary devices (e.g. CD drive, smart card, camera, microphone)
  - Disable Windows + R (Run)
  - Block execution of USB sticks
  - Disable network devices
  - Deactivate unneeded services
  - Disable / block mobile hotspot
  - Disable OS Update
  - Ensure NTFS is used on all volumes
  - Enable UAC (User Account Control)
  - Enable DEP (Data Execution Prevention)
  - Prevent ICMP tunnels
  - Prevent Teredo tunneling
  - Prevent adding printers (incl. IPP)
  - Enable Enhanced Protected Mode (EPM) for browsers
  - Enable auditing (define event logs large enough, min. 200MB)
    - Configure Account Logon audit policy
    - Configure Account Management audit policy
    - Configure Logon/Logoff audit policy
    - Configure Policy Change audit policy
    - Configure Privilege Use audit policy
  - Prevent the creation or use of Microsoft Account
  - Automatically lock PC when inactive (possibly via screen saver)
  - Prevent sending of unencrypted passwords to SMB shares
  - Block remote registry access
  - Prevent RDP access
  - Prevent automatic administrative logon to Recovery Console
- Configure Windows Defender (Windows Defender Advanced Threat Protection if applicable).
- Defender: turn off cloud-based protection and automatic submission of samples
- Implement application whitelisting
- Implement secure deletion of files and directories
- Implement appropriate authorisation concept (least privilege principle)
  - Restrict access to the PC to authenticated users and administrators
  - Deactivate guest accounts
  - Users with standard rights (users)
  - Complex passwords

## Generic security measures – external storage media

### Technical measures – external storage media

- Use a USB stick with hardware encryption
- Use a USB stick that is protected against the malware BadUSB (e.g. SafeToGo, Ironkey, datAshur)

## Recommendation by SwissPost

There are now many encrypted USB sticks on the market. These sticks are completely monitored from conception, through production, to delivery. The BSI has issued additional recommendations for the use of USB sticks. Even these sticks recommended by the BSI only meet the security requirements to a limited extent if a keylogger has been installed in the background.

For this reason, SwissPost recommends that the cantons use a USB stick with an independent power supply and keyboard, from datAshur. These can be encrypted and decrypted independently of the SDM hardware and thus further increase security.

## Organizational measures

- Do not make laptops and storage media publicly accessible
- Set up the device only in a secure environment behind IPS and/or proxy
- Carry out a security check of the fully configured device (once)
- Update the OS and the installed programs (SDM, Java) only in a secure environment behind IPS and/or proxy
  Special case: no OS update for the offline PC (all network connections are deactivated)
- Deactivate Java update
- Do not install Office programs
- Do not install a mail client
- Do not install any additional programs
- Mark devices physically and unambiguously (e.g. seal adhesive)
- Use devices (PC, USB sticks) only for the intended SDM applications
- Do not leave devices (PC, USB sticks) unattended (if you do, secure them with sealing glue)
- Define processes for key management when using cryptological procedures
- Define the process for handling devices before, during and after a vote
- Define the process for security-relevant events
- Integrate the devices into a lifecycle management process

## Recommendation for the secure handling of the system keys

For the operation of the e-voting solution, system keys and client certificates are stored locally on the cantons’ PCs (Online SDM and Offline SDM). To ensure maximum protection, these must be updated after each ballot, and at the latest, before the expiry date is reached. They are created by SwissPost and transferred to the cantons.
The following keys are required:

- 17 system keys for the operation of the SDM
- 3 client certificates
- 1 tenant

The following passwords are included:

- 1 password to client certificate
- 1 password to the tenant

The following procedure applies to the transmission:

1. The keys are compressed in a folder and protected with a password (e.g. with 7-Zip)
2. The passwords for client certificate and tenant are compressed in a separate document and password protected
3. The two ZIP files are each sent in a separate message via IncaMail
4. The password for the ZIP files is sent separately by SMS to another address (mobile phone)
5. The hash values are sent in a separate e-mail, together with instructions on how to check them

Alternatively, the keys can also be handed over to the cantonal officer in person using a secure USB stick.
