# [Architecture of the Swiss Post Voting System](SwissPost_Voting_System_architecture_document.pdf)

## What is the content of this document?

Architecture documentation of the e-voting system based on the arc42 architecture documentation style. In case this is unfamiliar the arc42 site provides a concise overview and examples which are worth taking a moment to review.

# [Specification of the Swiss Post Voting System](System_Specification.pdf)

## What is the content of this document?

The system specification provides a detailed specification of the cryptographic protocol—from the configuration phase to the voting phase to the counting phase—and includes pseudo-code representations of most algorithms. The document has the following target audiences:

* developers implementing the Swiss Post Voting System.
* reviewers checking that the system specification matches the  [computational proof](https://gitlab.com/swisspost-evoting/documentation/-/tree/master/Protocol).
* reviewers checking that the implementation (source code) matches the system specification.

## Why is a detailed specification of the cryptographic protocol important?

[Haenni et al](https://arbor.bfh.ch/13834/) stressed the importance of a precise system specification—using mathematical formalism and pseudo-code descriptions of algorithms—to reduce implementation errors and increase auditability of an e-voting system.

The system specification details all relevant cryptographic operations of the cryptographic protocol and communications between protocol participants. The pseudo-code representations are sufficiently precise to leave no room for interpretation while being independent of any programming language.

Moreover, the system specification links the [computational proof](https://gitlab.com/swisspost-evoting/documentation/-/tree/master/Protocol) which formally proves verifiability and vote secrecy to the actual source code.

## Changes since publication 2019

We have improved the system specification in multiple ways since the initial publication of 2019:

* Completeness: We provide explicit algorithms for representing and converting basic data types.
* Clarity: Pseudo-code algorithms detail the domain of input and output algorithms and provide a mathematically precise description of the operations.
* Consistency: Method and variable names align more closely to the [computational proof](https://gitlab.com/swisspost-evoting/documentation/-/tree/master/Protocol).
* Structure: The specification describes low-level algorithms separately and references them in high-level algorithms; therefore avoiding repetition.
* Readability: Sequence diagrams and overview tables increase the specificiation's readability.
* Separation of concerns: Each chapter focuses on a main aspect of the protocol. For instance, we moved the description on channel security (using digital signatures) to a separate chapter.

## Changes in version 0.9.6

Version 0.9.6 of the system specification contains the following changes:

* Strengthened  the protocol to verify sent-as-intended properties and vote correctness online in the control components. See Gitlab issue [#7](https://gitlab.com/swisspost-evoting/documentation/-/issues/7) for further explanations.
* Specified a recovery scenario for incorrectly inserted or discarded votes (tally phase). See Gitlab issue [#8](https://gitlab.com/swisspost-evoting/documentation/-/issues/8) for further explanations.
* Expanded the section on two-round return code schemes with alternative scenarios.
* Explained the election context and its relevance to the protocol's algorithms.
* Moved the GetEncryptionParameters algorithm to the crypto-primitives specification.
* Streamlined the usage and name of lists between the computational proofs and the system specifications.
* Improved clarity of sequence diagrams.
* Fixed smaller typos and erros in various sections.

## Open Issues

The current version of the system specification contains the following open issues:

* Write-in votes. Certain elections in Switzerland give voters the possibility to write the name of a candidate in a form instead of selecting a predefined candidate (so called write-in candidates). Usually, only a small fraction of voters select write-in candidates.
The protocol supports write-in candidates; however, the protocol cannot provide individual verifiability for the write-in candidates, since it is impossible to map all possible write-in values to a Choice Return Code. Future versions of the document will describe how we encode and decode write-in candidates and include them as part of a multi-recipient ElGamal ciphertext. Carsten Schürmann raised this issue in his maturity analysis of the system documentation (see Gitlab issue [#9](https://gitlab.com/swisspost-evoting/documentation/-/issues/9)).
* Logging. We currently omit how the control components securely store information in immutable chain of logs. Carsten Schürmann raised this issue in his maturity analysis of the system documentation (see Gitlab issue [#9](https://gitlab.com/swisspost-evoting/documentation/-/issues/9)).
* Voter authentication. We do not describe how the voting client authenticates to the voting server.
* Constitution of the electoral board and the administration board.
* Keystore algorithms. Private keys are stored in keystores. Currently, we do not describe how to open and seal keystores.

## Future Work

In future versions, we plan to address the following issue:

* Performance optimizations such as pre-computations in the voting client.

## Limitations

We would like to mention the following limitations of the system specification:

* The system specification does **not** provide a formal proof of verifiability and vote secrecy. Please check the [computational proof document](https://gitlab.com/swisspost-evoting/documentation/-/tree/master/Protocol) if you are looking for a security analysis.
* The system specification is bound to the [same limitations](https://gitlab.com/swisspost-evoting/documentation/-/tree/master/Protocol#limitations) than the computational proof regarding quantum computers, a trustworthy voting client for vote secrecy, and a trustworthy print office.
* The system specification does **not** describe which programming language and frameworks implement the protocol, the principles and design patterns that the source code follows, and the technical and organization constraints that the solution is bound to. These aspects will be covered in the architecture documentation.
* The system specification does **not** detail the verifier's algorithms, which will be described in the verifier specification.
