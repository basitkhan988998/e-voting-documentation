# [Test Concept of the Swiss Post Voting System](Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)

## What is the content of this document?

This document describes the test concept for the e-voting service. The test concept shows the test procedure, defines the cooperation and the responsibilities of all persons involved in the test process.
